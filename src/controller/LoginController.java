package controller;

import network.AvionControllerTCP;
import view.AvionView;
import view.LoginView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * Created by Anton
 */

public class LoginController extends Thread implements ActionListener {

    private LoginView loginView;
    private AvionView newWindow;

    private AvionControllerTCP avionControllerTCP;

    public LoginController(LoginView loginView) {
        this.loginView = loginView;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("SUBMIT")) {
            loginView.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            avionControllerTCP = new AvionControllerTCP();

            try {
                if(avionControllerTCP.startService()){

                    String referenciaAvion = loginView.getJtfUsername().getText();
                    int pincode = (int)Integer.valueOf(String.valueOf(loginView.getJtfPassword().getPassword())) ;
                    String referenciaVuelo = loginView.getJtfRefVuelo().getText();
                    boolean bool = avionControllerTCP.checkLogin(referenciaAvion, pincode, referenciaVuelo);
                    if (bool) {
                        loginView.setVisible(false);
                        AvionView newWindow = new AvionView(avionControllerTCP);
                        AvionViewController avionViewController = new AvionViewController(newWindow, avionControllerTCP , newWindow.getVentanaUpdateRetraso(), newWindow.getVentanaInfoVueloActual(),newWindow.getPanelGrafica());
                        newWindow.setController(avionViewController);
                        newWindow.setVisible(true);
                    } else {
                        System.out.println("enter the valid username and password");
                        JOptionPane.showMessageDialog(loginView, "Incorrect login or password",
                                "Error", JOptionPane.ERROR_MESSAGE);
                        System.exit(0);
                    }

                }else  {
                    System.out.println("enter the valid username and password");
                    JOptionPane.showMessageDialog(loginView, "Server is currently down, please try again later",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    System.exit(0);
                }

            } catch (IOException e1) {
                e1.printStackTrace();
                JOptionPane.showMessageDialog(loginView, "The server is temporary out of service , please try again later",
                        "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}
