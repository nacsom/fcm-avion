package controller;

import network.AvionControllerTCP;
import view.AvionView;
import view.VentanaGrafica;
import view.VentanaInfoVueloActual;
import view.VentanaUpdateRetraso;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * Created by Anton
 */
public class AvionViewController  implements ActionListener {

    private AvionView avionView;
    private AvionControllerTCP avionController;
    private VentanaUpdateRetraso ventanaUpdateRetraso;
    private VentanaInfoVueloActual ventanaInfoVueloActual;
    private VentanaGrafica ventanaGrafica;


    public AvionViewController(AvionView avionView, AvionControllerTCP avionControllerTCP, VentanaUpdateRetraso ventanaUpdateRetraso, VentanaInfoVueloActual ventanaInfoVueloActual, VentanaGrafica panelGrafica) {
        this.avionView = avionView;
        this.avionController =  avionControllerTCP;
        this.ventanaUpdateRetraso = ventanaUpdateRetraso;
        this.ventanaInfoVueloActual = ventanaInfoVueloActual;
        this.ventanaGrafica = panelGrafica;
    }



    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("UPDATE")) {
            try {
                int valor = (int) avionView.getJtfRetraso().getValue();
                if (avionController.updateRetraso(valor, (int)Integer.valueOf(String.valueOf(avionView.getJtfPincode().getPassword())))) {
                    avionController.setRetrasoEstadoVuelo((int)Integer.valueOf(String.valueOf(avionView.getJtfPincode().getPassword())));
                    ventanaGrafica.setRetraso(valor);
                    ventanaUpdateRetraso.setRetraso(valor);
                    if(valor == 0){
                        ventanaInfoVueloActual.setEstadoActualVuelo("ON-TIME");
                    }else{
                        ventanaInfoVueloActual.setEstadoActualVuelo("DELAYED");
                    }

                } else {
                    JOptionPane.showMessageDialog(avionView, "Incorrect Flight Pincode",
                            "Error", JOptionPane.ERROR_MESSAGE);
                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }

        }
    }

}
