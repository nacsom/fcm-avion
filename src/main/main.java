package main;

import controller.LoginController;
import view.LoginView;
import javax.swing.*;

/**
 * Created by Anton
 */
public class main {
    public static void main(String[] args) {
        try        {
            LoginView ventanaLogin=new LoginView();
            LoginController loginController = new LoginController(ventanaLogin);
            ventanaLogin.setController(loginController);

        }
        catch(Exception e)        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

    }
}
