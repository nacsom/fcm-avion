package network;

import model.EstadoAvion;

import java.io.*;
import java.net.Socket;

/**
 * Created by Anton
 */

public class AvionControllerTCP extends Thread {

    private Socket socket;
    private DataOutputStream doStream;
    private DataInputStream diStream;
    private ObjectInputStream objectInputStream;
    private ObjectOutputStream objectOutputStream;
    private EstadoAvion estadoAvion;
    private NetworkConfiguration networkConfiguration;

    private static final String IP = "localhost";
    private static final int PORT = 5555;
    private boolean estado = true;

    private int port;



    public Boolean startService() throws IOException {
        // Establim la connexio amb el servidor i enviem el missatge
        this.networkConfiguration = new NetworkConfiguration();
        socket = new Socket(networkConfiguration.getSERVER_IP(), networkConfiguration.getPORT_AVION());
        this.diStream = new DataInputStream(socket.getInputStream());
        this.doStream = new DataOutputStream(socket.getOutputStream());
        return true;
    }

    public void run() {
        while (estado) {
        }
        try {
            disconnectService();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void disconnectService() throws IOException {
        Boolean bool = diStream.readBoolean();
        if(bool) {
            doStream.writeUTF("LOGOUT");
            socket.close();
        }
    }

    public Boolean checkLogin(String referenciaAvion, int pincode, String referenciaVuelo) throws IOException {
        Boolean estado = false;
        doStream.writeUTF("LOGIN");
        doStream.writeUTF(referenciaAvion);
        doStream.writeInt(pincode);
        doStream.writeUTF(referenciaVuelo);
        boolean bool = diStream.readBoolean();
        if(bool){
            estado = true;
            port = diStream.readInt();
            this.socket.close();
            this.socket = new Socket(networkConfiguration.getSERVER_IP(), port);
            this.objectInputStream = new ObjectInputStream(socket.getInputStream());
            this.objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            try {
                this.estadoAvion =  (EstadoAvion) objectInputStream.readObject();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e){
                e.printStackTrace();
            }
        }
        return estado;
    }

    public boolean updateRetraso(int retraso, int pincode) throws IOException {
        this.diStream = new DataInputStream(socket.getInputStream());
        this.doStream = new DataOutputStream(socket.getOutputStream());
        Boolean estado = false;
        doStream.writeUTF("UPDATE");
        Boolean bool = diStream.readBoolean();
        if(bool){
            doStream.writeUTF(estadoAvion.getReferenciaVuelo());
            doStream.writeInt(pincode);
            bool = diStream.readBoolean();
            if(bool){
                doStream.writeInt(retraso);
                estado = true;
            }
        }
        return estado;
    }

    public void updateEstadoVuelo (String estadoVuelo) throws IOException {

        this.diStream = new DataInputStream(socket.getInputStream());
        this.doStream = new DataOutputStream(socket.getOutputStream());
        doStream.writeUTF("ESTADO");
        Boolean bool = diStream.readBoolean();
        if(bool){
            doStream.writeUTF(estadoVuelo);

        }
    }

    public EstadoAvion getEstadoAvion() {
        return this.estadoAvion;
    }

    public void setRetrasoEstadoVuelo(int retraso){
        this.estadoAvion.setRetrasoVuelo(retraso);
    }
}
