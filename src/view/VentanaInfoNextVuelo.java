package view;

import model.Vuelo;
import network.AvionControllerTCP;
import model.EstadoAvion;

import javax.swing.*;
import java.awt.*;


/**
 * Created by Anton
 */

public class VentanaInfoNextVuelo extends JPanel {



    // private JLabel jlSetAeropuertoOrigen;
    private JLabel jlAeropuertoOrigen;
    private JLabel jlAeropuertoDestino;
    // private JLabel jlSetAeropuertoDestino;
    private JLabel jlHoraDeSalida;
    //private JLabel jlSetHoraDeSalida;
    private JLabel jlNumeroDePasajeros;
    //private JLabel jlSetNumeroDePasajeros;
    private JPanel jpInformacionNextVuelo;

    ;
    public VentanaInfoNextVuelo(AvionControllerTCP avionControllerTCP) {

        EstadoAvion estadoAvion = avionControllerTCP.getEstadoAvion();
        Vuelo vuelo = estadoAvion.getVuelo();

        jpInformacionNextVuelo = new JPanel(new GridLayout(4, 1));
        if(vuelo.getReferenciaOrigen() == null){
            jlAeropuertoOrigen = new JLabel("Aeropuerto Origen: NOT ASSIGNED ");
            jlAeropuertoDestino = new JLabel("Aeropuerto Destino:  NOT ASSIGNED" );
            jlHoraDeSalida = new JLabel("Hora de salida:  NOT ASSIGNED" );
            jlNumeroDePasajeros = new JLabel("Numero de pasajeros:  NOT ASSIGNED" );
        }else{
            jlAeropuertoOrigen = new JLabel("Aeropuerto Origen: " + vuelo.getReferenciaOrigen() + "");
            jlAeropuertoDestino = new JLabel("Aeropuerto Destino: " + vuelo.getGetReferenciaDestina() + "");
            jlHoraDeSalida = new JLabel("Hora de salida: " + vuelo.getFechaSalida().substring(0, estadoAvion.getFechaSalida().length() - 2) + "");
            jlNumeroDePasajeros = new JLabel("Numero de pasajeros: " + vuelo.getPlazasDisponibles() + "");
        }
        


        jpInformacionNextVuelo.add(jlAeropuertoOrigen);

        jpInformacionNextVuelo.add(jlAeropuertoDestino);

        jpInformacionNextVuelo.add(jlHoraDeSalida);

        jpInformacionNextVuelo.add(jlNumeroDePasajeros);

        this.add(jpInformacionNextVuelo);


        this.setVisible(true);

    }
    public void mostrarAeropuertoOrigen(String aeropuertoOrigen) {
        jlAeropuertoOrigen.setText(" Aeropuerto Origen: "+aeropuertoOrigen);
    }

    public void mostrarAeropuertoDestino(String aeropuertoDestino) {
        jlAeropuertoDestino.setText(" Aeropuerto Destino: "+aeropuertoDestino);
    }

    public void mostrarHoraDeSalida(String HoraDeSalida) {
        jlHoraDeSalida.setText("Hora de Salida: "+HoraDeSalida);
    }

    public void mostrarNumeroDePasajeros(int numeroPasajeros) {
        jlNumeroDePasajeros.setText(String.valueOf("Numero de pasajeros: "+numeroPasajeros));
    }
}
