package view;

import network.AvionControllerTCP;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;


/**
 * Created by Anton
 */
public class VentanaGrafica extends JPanel implements Runnable{

    private static final int MAX_SCORE = 20;
    private static final int PREF_W = 800;
    private static final int PREF_H = 650;
    private static final int LONG_H = 30;
    private static final Color GRAPH_COLOR = Color.green;
    private static final Color GRAPH_POINT_COLOR = new Color(150, 50, 50, 180);
    private static final Stroke GRAPH_STROKE = new BasicStroke(3f);
    private static final int GRAPH_POINT_WIDTH = 12;
    private static final int LONG_Y = 25;
    private int ancho;
    private int alto;

    private int LONG_rallaH = LONG_H * 27;
    private int LONG_rallaY = 0;

    private int alturaCrucero; //Esta al voltant de [20.000 - 40.0000]
    private BufferedImage image;

    private int m, i, x, i2, i1;
    private int tramo1, tramo2, tramo3;
    private boolean vueloEnCurso = false, haLogeadoTarde = false, fasMoltTard = false;

    private Thread animator;

    // 1 MINUTO --> 180 DELAY , UN RECORRIDO ENTERO POR ARRIBA //
    //DELAY 700 --> 18.000 PEUS/MIN //
    private float DELAY;   //VELOCITAT DE REFRESH
    private String fechaSalida, estado;
    private int duracion, retraso, counter = 0;

    private int a = 0;

    private AvionControllerTCP avionControllerTCP;
    private VentanaInfoVueloActual ventanaInfoVueloActual;

    public VentanaGrafica(int ancho, int alto, AvionControllerTCP avionControllerTCP, VentanaInfoVueloActual ventanaInfoVueloActual){
        this.ventanaInfoVueloActual = ventanaInfoVueloActual;
        this.avionControllerTCP = avionControllerTCP;
        this.fechaSalida = avionControllerTCP.getEstadoAvion().getFechaSalida();
        this.fechaSalida = fechaSalida.substring(0, fechaSalida.length() - 2);   //Treiem el "." dels segons (float segons)
        this.duracion = avionControllerTCP.getEstadoAvion().getDuracionVuelo();
        this.alturaCrucero = avionControllerTCP.getEstadoAvion().getAlturaCrucero();
        this.DELAY = (alturaCrucero*700)/18000;    //Tanto tiempo de subida como altura tiene que subir. Sabemos que 700 DELAY son 18000peus/min
        this.ancho = ancho;
        this.alto = alto;
        this.tramo1 = 1;
        this.tramo2 = 0;
        this.tramo3 = 0;
        ventanaInfoVueloActual.setEstadoActualVuelo("AT-GATE");
        comprovaSiHaEntratTard();

        try {
            image = ImageIO.read(new File("imagen/avion_perfil.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addNotify() {
        super.addNotify();
        animator = new Thread(this);
        animator.start();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D)g;
        g2d.drawImage(image, i, x, 40, 40, null);
        drawGraph(g2d);
        g.dispose();
    }
    public void cycle() {
        if(tramo1 == 1) {
            ventanaInfoVueloActual.setEstadoActualVuelo("DEPARTURE");  //DESPEGUE
            estado = "DEPARTURE";
            m = Integer.valueOf(((alturaCrucero/1000)*6)/(LONG_H*2));
            x = 271 - i1*m;
            i++;
            i1++;
            if (x == 271 - (alturaCrucero/1000)*6) {
                i = LONG_H * 3;
                tramo1 = 0;
                tramo2 = 1;
                x = 271 - (alturaCrucero / 1000) * 6;
                this.DELAY = (180/2)*duracion; //180/2  //350
                ventanaInfoVueloActual.setEstadoActualVuelo("ON-TIME"); //VOLANT
                estado="ON-TIME";
            }
        }else if(tramo2 == 1){
            x = 271 - (alturaCrucero/1000)*6;
            if(retraso > 0){
                estado = "DELAYED";
                ventanaInfoVueloActual.setEstadoActualVuelo("DELAYED");
                this.DELAY = 180;
                a++;
                if(a >= (350)*retraso) {  //350 retraso 1min
                    a = 0;
                    retraso = 0;
                    this.DELAY = (180/2)*duracion;  //Segueix amb la velocitat de creuer que portaves
                }
            }else{
                x = 271 - (alturaCrucero / 1000) * 6;
                i++;
                if(i == LONG_rallaH - LONG_H*2){
                    i = LONG_rallaH - LONG_H*2;
                    i2 = 0;
                    tramo1 = 0;
                    tramo2 = 0;
                    tramo3 = 1;
                    x = 271 - (alturaCrucero/1000)*6;
                    this.DELAY = (alturaCrucero*700)/18000;    //Tanto tiempo de subida como altura tiene que subir. Sabemos que 700 DELAY son 18000peus/min
                    ventanaInfoVueloActual.setEstadoActualVuelo("LANDING"); //ATERRANT
                    estado = "LANDING";
                }
            }
        }else if(tramo3 == 1){
            m = Integer.valueOf(((alturaCrucero/1000)*6)/(LONG_H*2));
            x = (271-(alturaCrucero/1000)*6) + i2*m;
            i++;
            i2++;   //Comptador paral·lel que està a l'origen (es com al Tramo1)
            if(x >= 271){
                tramo3 = 0;
                tramo2 = 0;
                tramo1 = 1;
                i = LONG_H;
                i1 = 0;
                vueloEnCurso = false; //Ja ha arribat al destí
                ventanaInfoVueloActual.setEstadoActualVuelo("LANDED");
                fasMoltTard = true;
                estado = "LANDED";
            }
        }
    }

    public void drawAlturaCruceroLine(Graphics2D g2d){
        int x0 = LONG_H * 3;
        int x1 = LONG_rallaH - LONG_H*2;
        int y1, y0;
        y0 = y1 = 271 - (alturaCrucero/1000)*5;
        g2d.setColor(Color.RED.brighter());
        g2d.setStroke(new BasicStroke(4));
        g2d.drawLine(x0,y0,x1,y1);

        x0 = LONG_H;
        y0 = 271;
        x1 = LONG_H*3;
        y1 = 271 - (alturaCrucero/1000)*5;
        g2d.drawLine(x0,y0,x1,y1);

        x0 = LONG_rallaH - LONG_H*2;
        x1 = LONG_rallaH;
        y0 = 271 - (alturaCrucero/1000)*5;
        y1 = 271;
        g2d.drawLine(x0,y0,x1,y1);
    }

    @Override
    public void run() {
        while(true) {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            long beforeTime, timeDiff, sleep;

            beforeTime = System.currentTimeMillis();
            if(fasMoltTard) while(true){}   //Si no ha arribat a l'hora, no facis res.
            if(!haLogeadoTarde) {   //Si ha logeado a su hora, esperate a que llegue la hora de salida..
                while (!fechaSalida.equals(sdf.format(cal.getTime()))) {
                    cal = Calendar.getInstance();
                    sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                } //Espera't a que arribi l'hora de sortida.
            }
            vueloEnCurso = true;
            while (vueloEnCurso) {
                cycle();
                repaint();

                timeDiff = System.currentTimeMillis() - beforeTime;
                sleep = (long) (DELAY - timeDiff);

                if (sleep < 0)
                    sleep = 2;
                try {
                    Thread.sleep(sleep);
                    avionControllerTCP.updateEstadoVuelo(estado);
                    } catch (InterruptedException e) {
                    e.printStackTrace();
                    System.out.println("interrupted");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                beforeTime = System.currentTimeMillis();
            }
        }
    }

    public void drawGraph(Graphics2D g2d){
        g2d.setColor(Color.BLACK.brighter());
        g2d.setStroke(new BasicStroke(1));

        g2d.drawLine(LONG_H, alto - LONG_H, LONG_H, LONG_H);
        g2d.drawLine(LONG_H, alto - LONG_H, getWidth() - LONG_H, alto - LONG_H);

        // create hatch marks for y axis.
        for (int i = 0; i < LONG_Y; i++) {
            int x0 = LONG_H;
            //  int x1 = ancho - LONG_H;
            int y0 = alto - (((i + 1) * (alto - LONG_H * 2)) / LONG_Y + LONG_H);
            int x1 = LONG_rallaH+LONG_H*2;
            int y1 = y0;
            g2d.drawLine(x0, y0, x1, y1);
        }
        g2d.drawLine(ancho-LONG_H, alto - LONG_H , ancho - LONG_H, LONG_Y);
        g2d.drawLine(ancho-LONG_H, alto - LONG_H , ancho - LONG_H, LONG_Y);

        drawAlturaCruceroLine(g2d);
    }

    public void comprovaSiHaEntratTard(){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String fechaDiaMesHoraVuelo = fechaSalida.substring(0, fechaSalida.length() - 6);
        if(fechaDiaMesHoraVuelo.equals(sdf.format(cal.getTime()).substring(0, sdf.format(cal.getTime()).length() - 6))){
            int minutoSalidaVuelo = Integer.valueOf(fechaSalida.substring(14, fechaSalida.length() - 3));
            int segundoSalidaVuelo = Integer.valueOf(fechaSalida.substring(17, fechaSalida.length()));
            int minutoActual = Integer.valueOf(sdf.format(cal.getTime()).substring(14, sdf.format(cal.getTime()).length() - 3));
            int segundoActual = Integer.valueOf(sdf.format(cal.getTime()).substring(17, sdf.format(cal.getTime()).length()));
            int tiempoSubida = alturaCrucero/18000;
            int diferenciaEnSegundos = (minutoSalidaVuelo*60 + segundoSalidaVuelo) - (minutoActual*60 + segundoActual);
            if(diferenciaEnSegundos > 0){
                //EL PILOT HA FET LOGIN ABANS QUE SORTIS EL VOL i per tant correrà tot amb normalitat
                i = LONG_H;
                i1 = 0;
                m = Integer.valueOf(((alturaCrucero/1000)*6)/(LONG_H*2));
                x = 271;
            }else {
                //EL PILOT HA FET TARD..
                haLogeadoTarde = true;
              /*  System.out.println("Min actual: " + minutoActual);
                System.out.println("Min salida: " + minutoSalidaVuelo);
                System.out.println("Time subida: " + tiempoSubida);*/
                diferenciaEnSegundos = -((minutoSalidaVuelo*60 + segundoSalidaVuelo) - (minutoActual*60 + segundoActual));
                if (diferenciaEnSegundos < (duracion*60 + tiempoSubida*2*60)) {    //Si està dintre del temps que hauria d'estar volant..
                    if((diferenciaEnSegundos > tiempoSubida*60 && diferenciaEnSegundos < duracion + tiempoSubida*60) || diferenciaEnSegundos <= tiempoSubida*60){
                        //Cal aproximar on està del tram creuer..
                        //diferenciaEnSegundos = Math.abs(diferenciaEnSegundos);   //Li restem la pujada i la baixada. Ara treballem en positiu.
                        tramo1 = 0;
                        tramo2 = 1;
                        tramo3 = 0;
                        int duracionSegundos = duracion*60;
                        x = 271 - (alturaCrucero / 1000) * 6;
                        this.DELAY = (180/2)*duracion; //180/2  //350
                        ventanaInfoVueloActual.setEstadoActualVuelo("ON-TIME"); //VOLANT
                        estado="ON-TIME";
                        if((diferenciaEnSegundos) < duracionSegundos/4){
                            i = LONG_H*3;
                        }
                        else if(diferenciaEnSegundos < duracionSegundos/3){
                            i = 660/4 + LONG_H*3;
                        }
                        else if(diferenciaEnSegundos < duracionSegundos/2) {
                            i = 660/3 + LONG_H*3;
                        }
                        else if(diferenciaEnSegundos < duracionSegundos){
                            i = 660/2 + LONG_H*3;
                        }
                    }else if(-diferenciaEnSegundos < duracion + tiempoSubida*60){   //L'avio està al tram final. El col·loquem a l'inici del tram final
                        tramo1 = 0;
                        tramo2 = 0;
                        tramo3 = 1;
                        i = LONG_rallaH - LONG_H*2;
                        i2 = 0;
                        x = 271 - (alturaCrucero/1000)*6;
                        this.DELAY = (alturaCrucero*700)/18000;    //Tanto tiempo de subida como altura tiene que subir. Sabemos que 700 DELAY son 18000peus/min
                        ventanaInfoVueloActual.setEstadoActualVuelo("LANDING"); //ATERRANT
                        estado = "LANDING";
                    }
                }else{
                    fasMoltTard = true;
                    i = LONG_H;
                    x = 271;
                }
            }
        }else{
            estado = "ON-TIME";
            i = LONG_H;
            x = 271;
        }
    }

    public void setRetraso(int retraso) {
        this.retraso = retraso;
    }

    public int getAlturaCrucero() {
        return alturaCrucero;
    }

    public void setAlturaCrucero(int alturaCrucero) {
        this.alturaCrucero = alturaCrucero;
    }
}
