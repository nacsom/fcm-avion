package view;

import model.EstadoAvion;
import model.Usuario;
import network.AvionControllerTCP;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;


/**
 * Created by Anton */
public class VentanaInfoPasajeros extends JPanel {


  public VentanaInfoPasajeros(AvionControllerTCP avionControllerTcp){



      EstadoAvion estadoAvion = avionControllerTcp.getEstadoAvion();
      LinkedList<Usuario> usuarios = estadoAvion.getUsuarios();
      Object[][] data = new Object[usuarios.size()][3];

      String[] colNames = {"Name","Last Name","Email"};
      int i = 0;
      for (Usuario user : usuarios){
          data[i][0] = user.getNom();
          data[i][1] = user.getApellido();
          data[i][2] = user.getEmail();
          i++;
      }

      JTable tableUsers = new JTable(data, colNames);
      tableUsers.setPreferredScrollableViewportSize(new Dimension(440, 160));

      JScrollPane scrollPane = new JScrollPane(tableUsers);
      this.add(scrollPane, BorderLayout.CENTER);


      this.setVisible(true);


  }
}




