package view;

import model.EstadoAvion;
import network.AvionControllerTCP;

import javax.swing.*;

/**
 * Created by Anton
 * */
public class VentanaInfoVueloActual extends JPanel {

    private JLabel jlEstadoActualVuelo;

    private JPanel jpInformacionVueloActual;

    public VentanaInfoVueloActual(AvionControllerTCP avionControllerTcp) {

        EstadoAvion estadoAvion = avionControllerTcp.getEstadoAvion();
        String estadoActualVuelo = estadoAvion.getEstadoVuelo();

        jpInformacionVueloActual= new JPanel();
        jlEstadoActualVuelo= new JLabel("Estado Actual del vuelo: " + estadoActualVuelo + "");

        jpInformacionVueloActual.add(jlEstadoActualVuelo);

        this.add(jpInformacionVueloActual);
        setVisible(true) ;
    }

    public void setEstadoActualVuelo(String status){
        jlEstadoActualVuelo.setText("Estado Actual del vuelo: " + status + "");
    }

}









