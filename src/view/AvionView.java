package view;

import controller.AvionViewController;
import network.AvionControllerTCP;

import javax.swing.*;
import java.awt.*;


/**
 * Created by Anton
 */
public class AvionView extends JFrame {

    private JPasswordField jtfPincode;
    private JPanel jpCompleto;
    private JButton submit;
    private JPanel jpParteCentroIzquierda;
    private JSpinner jtfRetraso;
    private VentanaUpdateRetraso ventanaUpdateRetraso;
    private VentanaInfoVueloActual ventanaInfoVueloActual;
    private VentanaGrafica panelGrafica;

    public AvionView(AvionControllerTCP avionControllerTCP){


        ventanaInfoVueloActual =  new VentanaInfoVueloActual(avionControllerTCP);

        VentanaInfoNextVuelo ventanaInfoNextVuelo = new VentanaInfoNextVuelo(avionControllerTCP);

        ventanaUpdateRetraso = new VentanaUpdateRetraso(avionControllerTCP);

        jpCompleto = new JPanel(new GridLayout(2,1));

        panelGrafica = new VentanaGrafica(900,290, avionControllerTCP, ventanaInfoVueloActual);
        panelGrafica.setBorder(BorderFactory.createTitledBorder("Grafica"));
        jpCompleto.add(panelGrafica,BorderLayout.NORTH);

        JPanel jpCentral= new JPanel (new GridLayout(1,2));



        jpParteCentroIzquierda= new JPanel(new GridLayout(3,1));
        jpParteCentroIzquierda.add(ventanaInfoNextVuelo,BorderLayout.NORTH);
        ventanaInfoNextVuelo.setBorder(BorderFactory.createTitledBorder("Informacion sobre el siguiente Vuelo"));
        ventanaInfoNextVuelo.setMinimumSize(new Dimension(500,300));

        jpParteCentroIzquierda.add(ventanaInfoVueloActual,BorderLayout.CENTER);
        ventanaInfoVueloActual.setBorder(BorderFactory.createTitledBorder("Informacion sobre el vuelo actual"));

        jpParteCentroIzquierda.add(ventanaUpdateRetraso,BorderLayout.SOUTH);
        ventanaUpdateRetraso.setBorder(BorderFactory.createTitledBorder("Informacion sobre el retraso del vuelo actual"));

        jpCentral.add(jpParteCentroIzquierda);


        VentanaInfoPasajeros ventanaInfoPasajeros = new VentanaInfoPasajeros (avionControllerTCP);
        jpCentral.add(ventanaInfoPasajeros,BorderLayout.CENTER);
        ventanaInfoPasajeros.setBorder(BorderFactory.createTitledBorder("Informacion sobre los pasajeros del vuelo actual"));


        jpCentral.setBorder(BorderFactory.createTitledBorder("Informacion sobre los pasajeros"));
        jpCompleto.add(jpCentral,BorderLayout.SOUTH);


        JPanel jpPartefinal = new JPanel(new GridLayout(1,5));

        submit= new JButton("Update Time");

        JLabel jlPincode= new JLabel(" Pincode");
        SpinnerNumberModel model3 = new SpinnerNumberModel(0,0,9999,1);
        jtfPincode= new JPasswordField();
        SpinnerNumberModel model4 = new SpinnerNumberModel(0,0,1440,1);
        JLabel jtlRetraso = new JLabel(" Retraso");
        jtfRetraso = new JSpinner(model4);

        jpPartefinal.add(jtlRetraso);
        jpPartefinal.add(jtfRetraso);
        jpPartefinal.add(jlPincode);
        jpPartefinal.add(jtfPincode);
        jpPartefinal.add(submit);

        JLabel Imagen= new JLabel((new ImageIcon("imagen/Prueba2.png")));
        //jpPartefinal.add(Imagen);

        this.add(jpCompleto, BorderLayout.CENTER);
        this.add(jpPartefinal,BorderLayout.SOUTH);


        this.setVisible(true);
        this.setResizable(false);
        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Estado Vuelo");
        setSize(950, 700);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


    }
    public void setController(AvionViewController controller) {
        submit.setActionCommand("UPDATE");
        submit.addActionListener(controller);

    }

    public VentanaUpdateRetraso getVentanaUpdateRetraso() {
        return ventanaUpdateRetraso;
    }

    public VentanaInfoVueloActual getVentanaInfoVueloActual() {
        return ventanaInfoVueloActual;
    }

    public JPasswordField getJtfPincode() {
        return jtfPincode;
    }

    public JSpinner getJtfRetraso() {
        return jtfRetraso;
    }

    public VentanaGrafica getPanelGrafica() {
        return panelGrafica;
    }
}
