package view;


import controller.LoginController;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Anton
 */
public class LoginView extends JFrame{

    private JButton submit;
    private JPanel panel;
    private JLabel jlUsername, jlPassword, jlRefVuelo;
    private JTextField jtfUsername, jtfRefVuelo;
    private JPasswordField jtfPassword;
    private JLabel jlImagen;

    public LoginView(){

        panel = new JPanel(new GridLayout(4,1));
        panel.setBorder(BorderFactory.createTitledBorder("Registrarse"));

        jlUsername = new JLabel("Referencia Avion");
        jtfUsername = new JTextField(15);
        jlRefVuelo = new JLabel("Referencia Vuelo");
        jtfRefVuelo = new JTextField(15);
        jlPassword = new JLabel("Password");
       // SpinnerNumberModel model1 = new SpinnerNumberModel(1000,1000,9999,1);
        jtfPassword = new JPasswordField();

        submit = new JButton("SUBMIT");

        panel.add(jlUsername);
        panel.add(jtfUsername);
        panel.add(jlRefVuelo);
        panel.add(jtfRefVuelo);
        panel.add(jlPassword);
        panel.add(jtfPassword);
        panel.add(submit);

        this.add(panel, BorderLayout.NORTH);
        jlImagen= new JLabel((new ImageIcon("imagen/Avion.jpg")));

        this.add(jlImagen);
        //configuracion de la ventana
        setTitle("FCM-AVION");

        this.setSize(400, 500);
       // this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
    public void setController(LoginController controller) {
        submit.setActionCommand("SUBMIT");
        submit.addActionListener(controller);
    }


    public JTextField getJtfRefVuelo() {
        return jtfRefVuelo;
    }

    public JTextField getJtfUsername() {
        return jtfUsername;
    }

    public JPasswordField getJtfPassword() {
        return jtfPassword;
    }
}
