package view;

import model.EstadoAvion;
import network.AvionControllerTCP;

import javax.swing.*;
import java.awt.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Anton
 */
public class VentanaUpdateRetraso extends JPanel{

    private JPanel jpUpdateRetraso;
    private JLabel jlHoraSalidaVuelo;
    private JLabel jlHoraLlegadaVuelo;
    private JLabel jlRetraso;
    private int retraso;
    private EstadoAvion estadoAvion;
    private String fechaSalida;

    public VentanaUpdateRetraso(AvionControllerTCP avionControllerTCP){

        estadoAvion = avionControllerTCP.getEstadoAvion();
        fechaSalida = estadoAvion.getFechaSalida();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = null;
        try {
            Date d1 = df.parse(estadoAvion.getFechaSalida());
            cal = Calendar.getInstance();
            cal.setTime(d1);
            cal.add(Calendar.MINUTE, estadoAvion.getDuracionVuelo() + retraso +((estadoAvion.getAlturaCrucero()*2)/18000));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        jpUpdateRetraso= new JPanel(new GridLayout(3,1));
        jlHoraSalidaVuelo = new JLabel("Fecha y hora de la salida del vuelo: " + estadoAvion.getFechaSalida().substring(0, estadoAvion.getFechaSalida().length() - 2));
        jlHoraLlegadaVuelo= new JLabel("Fecha y hora de la llegada del vuelo: " + df.format(cal.getTime()));
        jlRetraso= new JLabel("Retraso previsto: " + retraso + " minutos");

        jpUpdateRetraso.add(jlHoraLlegadaVuelo);
        jpUpdateRetraso.add(jlHoraSalidaVuelo);
        jpUpdateRetraso.add(jlRetraso);
        this.add(jpUpdateRetraso);
        setVisible(true);
    }

    public void setRetraso(int retraso) {
        this.retraso = retraso;
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = null;
        try {
            Date d1 = df.parse(estadoAvion.getFechaSalida());
            cal = Calendar.getInstance();
            cal.setTime(d1);
            cal.add(Calendar.MINUTE, estadoAvion.getDuracionVuelo() + retraso + ((estadoAvion.getAlturaCrucero()*2)/18000));

        } catch (ParseException e) {
            e.printStackTrace();
        }
        jlHoraLlegadaVuelo.setText("Fecha y hora de la llegada del vuelo: " + df.format(cal.getTime()));
        jlRetraso.setText("Retraso previsto: " + retraso + " minutos");
    }
}


